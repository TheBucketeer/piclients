package client;

import client.errors.CommandQueueException;
import client.user.UserContext;
import client.util.Util;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.hyperledger.fabric.sdk.*;
import org.hyperledger.fabric.sdk.exception.*;
import org.hyperledger.fabric.sdk.identity.X509Enrollment;
import org.hyperledger.fabric.sdk.security.CryptoSuite;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.security.PrivateKey;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.regex.Pattern;

import static java.nio.charset.StandardCharsets.UTF_8;
import static shared.GlobalConfig.*;


public class PiClient {
    private static final boolean DEBUG = false;

    protected JSONObject jo;
    private HFClient hfClient;
    private Channel channel;
    ConcurrentLinkedQueue<JSONObject> jsonChaincodeEvents = new ConcurrentLinkedQueue<>();
    private final HashSet<String> receivedValidBlockEventTxIds = new HashSet<>();
    private final HashSet<String> receivedInvalidBlockEventTxIds = new HashSet<>();
    private final HashSet<String> receivedCCEventTxIds = new HashSet<>();
    private final HashMap<String, Long> txSendTimes = new HashMap<>();

    private String affiliation;
    private String ccName;
    private String signedCertFile;
    private String privateKeyFile;
    private String msp;
    private String currentNetworkId;
    private String username;

    protected String aId;
    protected String logFileName, eventLogFileName;

    public volatile boolean stopCondition;
    // Ignore kill events if they're part of the first composite event we receive
    private volatile boolean ignoreKillEvents = true;

    private final ConcurrentLinkedQueue<String> pendingTxs = new ConcurrentLinkedQueue<>();

    protected PiClient(String jsonConfigFile) throws IOException, InstantiationException, InvocationTargetException, NoSuchMethodException, InvalidArgumentException, IllegalAccessException, NetworkConfigurationException, CryptoException, ClassNotFoundException {
        this.setup(jsonConfigFile);
        this.setTxConfirmationListener();
    }

    private void setup(String jsonConfigFile) throws IOException, NetworkConfigurationException, InvalidArgumentException, IllegalAccessException, InvocationTargetException, InstantiationException, CryptoException, ClassNotFoundException, NoSuchMethodException {
        File jsonFile = new File(jsonConfigFile);
        this.jo = new JSONObject(FileUtils.readFileToString(jsonFile, "utf-8"));
        this.aId = this.jo.getString("asset_id");
        this.logFileName = this.jo.getString("logfile");
        this.eventLogFileName = this.jo.getString("eventlogfile");
        this.emptyLogFile(this.logFileName);
        this.emptyLogFile(this.eventLogFileName);
        NetworkConfig nc = NetworkConfig.fromJsonFile(new File(this.jo.getString("connection_profile")));
        this.affiliation = this.jo.getString("affiliation");
        this.ccName = this.jo.getJSONObject("chaincode").getString("name");
        this.privateKeyFile = this.jo.getString("keyfile");
        this.signedCertFile = this.jo.getString("certfile");
        this.username = this.jo.getString("username");
        this.msp = nc.getOrganizationInfo(this.jo.getString("organization")).getMspId();
        this.hfClient = HFClient.createNewInstance();
        this.hfClient.setCryptoSuite(CryptoSuite.Factory.getCryptoSuite());
        this.currentNetworkId = this.jo.getString("network_version");

        Util.setWorkingDirectory(this.jo.getString("working_directory"));
        UserContext userContext = Util.readUserContext(this.affiliation, this.username);
        if (userContext == null || userContext.getNetworkId() == null || !userContext.getNetworkId().equals(this.currentNetworkId)) {
            Util.cleanUp();
            this.p("Registering user " + this.username + " for " + this.affiliation);
            this.enrollExistingUser();
        } else {
            this.p("Found existing enrollment for " + this.username);
        }
        this.hfClient.setUserContext(Objects.requireNonNull(Util.readUserContext(this.affiliation, this.username)));
        String channelName = nc.getChannelNames().iterator().next();
        this.hfClient.loadChannelFromConfig(channelName, nc);
        this.channel = this.hfClient.getChannel(channelName);
    }

    void eventLog(String s) {
        logToFile(this.eventLogFileName, s);
    }

    private void emptyLogFile(String logFileName) {
        try {
            PrintStream fileStream = new PrintStream(new FileOutputStream(logFileName, false));
            fileStream.write((byte) '\n');
            fileStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void enrollExistingUser() throws IOException {
        UserContext userContext = new UserContext();
        userContext.setName(this.username);
        userContext.setAffiliation(this.affiliation);
        userContext.setNetworkId(this.currentNetworkId);
        userContext.setMspId(this.msp);

        FileInputStream stream = new FileInputStream(this.signedCertFile);
        String publicKeyStr = IOUtils.toString(stream, "UTF-8");
        stream.close();

        stream = new FileInputStream(this.privateKeyFile);
        String privateKeyStr = IOUtils.toString(stream, "UTF-8");
        stream.close();

        Reader pemReader = new StringReader(privateKeyStr);
        PEMParser pemParser = new PEMParser(pemReader);
        PrivateKeyInfo pemPair = (PrivateKeyInfo) pemParser.readObject();
        pemParser.close();
        PrivateKey privateKey = (new JcaPEMKeyConverter()).getPrivateKey(pemPair);
        userContext.setEnrollment(new X509Enrollment(privateKey, publicKeyStr));
        Util.writeUserContext(userContext);
    }

    protected void initChannel() throws BaseException {
        this.channel.initialize();
    }

    private void setEventListener(String eventName, ChaincodeEventListener chaincodeEventListener) throws InvalidArgumentException {
        this.channel.registerChaincodeEventListener(Pattern.compile(Pattern.quote(this.ccName)), Pattern.compile(Pattern.quote(eventName)), chaincodeEventListener);
    }

    // Register an event listener for BlockEvents, which will be used for detecting finished/failed transactions.
    private void setTxConfirmationListener() throws InvalidArgumentException {
        this.channel.registerBlockListener(this::processBlockEvent);
    }

    private void processBlockEvent(BlockEvent blockEvent) {
        for (BlockEvent.TransactionEvent txEvent : blockEvent.getTransactionEvents()) {
            String txId = txEvent.getTransactionID();
            // Is this event new to us?
            boolean isNew;
            if (txEvent.isValid()) {
                synchronized (this.receivedValidBlockEventTxIds) {
                    isNew = !this.receivedValidBlockEventTxIds.contains(txId);
                    if (isNew) {
                        this.receivedValidBlockEventTxIds.add(txId);
                    }
                }
            } else {
                synchronized (this.receivedInvalidBlockEventTxIds) {
                    isNew = !this.receivedInvalidBlockEventTxIds.contains(txId);
                    if (isNew) {
                        this.p("Detected invalid transaction " + txId + "!");
                        this.receivedInvalidBlockEventTxIds.add(txId);
                    }
                }
            }
            // If this event is about a pending transaction, we must mark it as done by removing it from the list (even if invalid)
            synchronized (this.pendingTxs) {
                if (isNew && this.pendingTxs.contains(txId)) {
                    this.p("Pending transaction " + txId + " completed");
                    this.pendingTxs.remove(txEvent.getTransactionID());
                }
            }
            // Print event response time
            synchronized (this.txSendTimes) {
                if (isNew && this.txSendTimes.containsKey(txId)) {
                    long currentTime = System.nanoTime();
                    long txSendTime = this.txSendTimes.get(txId);
                    this.p("Event response time for " + txId + " was " + (currentTime - txSendTime) + " nanoseconds!");
                }
            }
        }
    }

    private void addJsonChaincodeEvent(String key, JSONObject jo) {
        jo.put("eventName", key);
        this.jsonChaincodeEvents.add(jo);
    }

    // Sets an event listener that listens for composite events and adds their components to the client's queue while filtering out duplicates
    protected void setDefaultCompositeEventListener(String eventName) throws InvalidArgumentException {
        this.setEventListener(COMPOSITE_EVENT, (handle, blockEvent, chaincodeEvent) -> {
            this.processBlockEvent(blockEvent);
            JSONObject compositeEvent = new JSONObject(new String(chaincodeEvent.getPayload()));
            String txId = chaincodeEvent.getTxId();
            synchronized (this.receivedInvalidBlockEventTxIds) {
                // If the event is invalid, return
                if (this.receivedInvalidBlockEventTxIds.contains(txId)) {
                    return;
                }
            }
            synchronized (this.receivedValidBlockEventTxIds) {
                // If the event is not invalid, processBlockEvent must have placed it in this list
                assert (this.receivedValidBlockEventTxIds.contains(txId));
            }
            // If we have received this event before, ignore it
            boolean isNew;
            synchronized (this.receivedCCEventTxIds) {
                isNew = !this.receivedCCEventTxIds.contains(txId);
                if (isNew) {
                    this.receivedCCEventTxIds.add(txId);
                }
            }
            if (isNew) {
                this.eventLog(compositeEvent.toString());
                JSONArray eventArray = compositeEvent.getJSONArray("events");
                for (int i = 0; i < eventArray.length(); i++) {
                    JSONObject eventJson = eventArray.getJSONObject(i);
                    if (eventName == null) {
                        // Store all events
                        Iterator<String> keysItr = eventJson.keys();
                        while (keysItr.hasNext()) {
                            String key = keysItr.next();
                            // Ignore kill events if needed
                            if (key.equals(KILL_EVENT) && this.ignoreKillEvents) {
                                continue;
                            }
                            this.eventLog("{\"" + key + "\": " + eventJson.getJSONObject(key) + "}");
                            this.addJsonChaincodeEvent(key, eventJson.getJSONObject(key));
                        }
                    } else {
                        if (eventJson.has(eventName)) {
                            // Store all events that have the name eventName
                            this.addJsonChaincodeEvent(eventName, eventJson.getJSONObject(eventName));
                            d(eventJson.getJSONObject(eventName));
                        }
                        if (eventJson.has(KILL_EVENT) && !this.ignoreKillEvents) {
                            this.addJsonChaincodeEvent(KILL_EVENT, eventJson.getJSONObject(KILL_EVENT));
                            d(eventJson.getJSONObject(KILL_EVENT));
                        }
                    }
                }
                // Stop ignoring kill events after processing the very first event received
                this.ignoreKillEvents = false;
            }
        });
    }

    // Wrapper for catching all events within composite events
    protected void setDefaultAnyCompositeEventListener() throws InvalidArgumentException {
        this.setDefaultCompositeEventListener(null);
    }

    protected String invokeCC(CC_FUNCTION function, String... args) {
        return this.invokeCC(function, this.channel.getPeers(EnumSet.of(Peer.PeerRole.ENDORSING_PEER)), args);
    }

    protected String invokeCC(CC_FUNCTION function, Collection<Peer> peers, String... args) {
        return this.invokeCC(function, peers, N_INVOCATION_RETRIES, args);
    }

    protected String invokeCC(CC_FUNCTION function, int nRetries, String... args) {
        return this.invokeCC(function, this.channel.getPeers(EnumSet.of(Peer.PeerRole.ENDORSING_PEER)), nRetries, args);
    }

    protected String invokeCC(CC_FUNCTION function, boolean blocking, String... args) {
        return this.invokeCC(function, this.channel.getPeers(EnumSet.of(Peer.PeerRole.ENDORSING_PEER)), blocking, args);
    }

    protected String invokeCC(CC_FUNCTION function, Collection<Peer> peers, int nRetries, String... args) {
        return this.invokeCC(function, peers, nRetries, true, args);
    }

    private String invokeCC(CC_FUNCTION function, Collection<Peer> peers, boolean blocking, String... args) {
        return this.invokeCC(function, peers, N_INVOCATION_RETRIES, blocking, args);
    }

    private String invokeCC(CC_FUNCTION function, int nRetries, boolean blocking, String... args) {
        return this.invokeCC(function, this.channel.getPeers(EnumSet.of(Peer.PeerRole.ENDORSING_PEER)), nRetries, blocking, args);
    }

    private String invokeCC(CC_FUNCTION function, Collection<Peer> peers, int nRetries, boolean blocking, String... args) {
        long startTime, stopTime;
        startTime = System.nanoTime();
        try {
            TransactionProposalRequest request = this.hfClient.newTransactionProposalRequest();
            ChaincodeID ccid = ChaincodeID.newBuilder().setName(this.ccName).build();
            request.setChaincodeID(ccid);
            request.setProposalWaitTime(1000000);
            request.setFcn(function.getName());
            request.setArgs(args);
            request.setChaincodeLanguage(TransactionRequest.Type.JAVA);
            Map<String, byte[]> tm = new HashMap<>();
            tm.put("HyperLedgerFabric", "TransactionProposalRequest:JavaSDK".getBytes(UTF_8));
            tm.put("method", "TransactionProposalRequest".getBytes(UTF_8));
            //tm.put("my_key", "my_value_string".getBytes(UTF_8));
            request.setTransientMap(tm);
            //ChaincodeEndorsementPolicy chaincodeEndorsementPolicy = new ChaincodeEndorsementPolicy();
            //chaincodeEndorsementPolicy.fromYamlFile();
            //request.setChaincodeEndorsementPolicy(chaincodeEndorsementPolicy);

            Collection<ProposalResponse> response = this.channel.sendTransactionProposal(request, peers);
            stopTime = System.nanoTime();
            String txId = response.iterator().next().getTransactionID();
            this.txSendTimes.put(txId, stopTime);
            this.p("Sending PROPOSAL for " + function.dumpInvocation(args) + "(" + txId + ") took " + (stopTime - startTime) + " nanoseconds!");
            return processProposalResponse(response, blocking);
        } catch (ProposalException e) {
            this.p(function.dumpInvocation(args) + " failed, key collision?  " + nRetries + " attempts to go!");
            if (nRetries == 0) {
                this.p(e.getMessage());
                e.printStackTrace();
//                throw e;
                return "";
            } else {
                return this.invokeCC(function, peers, nRetries - 1, blocking, args);
            }
        } catch (InvalidArgumentException e) {
            this.p(function.dumpInvocation(args) + " failed because it was invalid!");
            return function.dumpInvocation(args) + " failed because it was invalid!";
        }
    }

    private String processProposalResponse(Collection<ProposalResponse> response, boolean blocking) throws ProposalException {
        String result = null, txId = null;
        // What do all of these peers think of this idea?
        // TODO: ignore exceptions, attempt transaction send anyways - that's the whole point of a Blockchain!
        for (ProposalResponse pres : response) {
            result = pres.getMessage();
            txId = pres.getTransactionID();
            try {
                String stringResponse = new String(pres.getChaincodeActionResponsePayload());
                this.d("chaincodeActionResponsePayload: " + stringResponse);
            } catch (InvalidArgumentException e) {
                this.p("Transaction proposal FAILED on channel " + this.channel.getName() + " by peer " + pres.getPeer().getName() + "!\n");
                this.p("With message: " + pres.getMessage() + "\n" +
                        "Status: " + pres.getStatus() + "\n" +
                        "And transaction id: " + pres.getTransactionID());
            }
        }
        // Send off to the ordering service!
        this.pendingTxs.add(txId);
        this.channel.sendTransaction(response);
        long startWaitingTime = System.currentTimeMillis();
        if (blocking) {
            // Wait until the transaction has been sent to us in a BlockEvent, so we know it has been processed
            while (true) {
                synchronized (this.pendingTxs) {
                    if (!this.pendingTxs.contains(txId)) {
                        break;
                    }
                }
                long timeWaitedSeconds = (System.currentTimeMillis() - startWaitingTime) / 1000;
                if (timeWaitedSeconds > INVOCATION_TIMEOUT_S) {
                    throw new ProposalException("[CUSTOM] Timeout of " + INVOCATION_TIMEOUT_S + " seconds reached without transaction confirmation!");
                }
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ignored) {}
            }
            assert !(this.receivedValidBlockEventTxIds.contains(txId) && this.receivedInvalidBlockEventTxIds.contains(txId));
            assert this.receivedValidBlockEventTxIds.contains(txId) || this.receivedInvalidBlockEventTxIds.contains(txId);
            if (this.receivedInvalidBlockEventTxIds.contains(txId)) {
                throw new ProposalException("[CUSTOM] Transaction returned as invalid!");
            }
        }
        return result;
    }

    protected void run() throws InterruptedException, CommandQueueException {
        while (!this.stopCondition) {
            Thread.sleep(100);
        }
        this.destroy();
    }

    protected void destroy() {
        this.p("Stopped");
    }

    public String getAssetId() {
        return this.aId;
    }

    public void p(Object o) {
        this.p(o, false);
    }

    protected void p(Object o, boolean isPrompt) {
        if (o instanceof String && ((String) o).length() == 0) {
            return;
        }
        String output = "[" + this.getClass().getName() + "]: " + o;
        if (isPrompt) {
            System.out.print(output);
        } else {
            System.out.println(output);
        }
        System.out.flush();

        if (CLIENT_LOG_TO_FILE) {
            logToFile(this.logFileName, output + '\n');
        }
    }

    public static void logToFile(String fileName, String contents, boolean overWrite) {
        try {
            PrintStream fileStream = new PrintStream(new FileOutputStream(fileName, !overWrite));
            fileStream.append(contents).append('\n');
            fileStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void logToFile(String fileName, String contents) {
        logToFile(fileName, contents, false);
    }

    private void d(Object o) {
        if (DEBUG) {
            this.p(o);
        }
    }

    private void defaultSetup() throws BaseException {
        setDefaultAnyCompositeEventListener();
        initChannel();
    }

    public static void main(String[] args) throws Exception {
        String fileName;
        if (args.length == 0) {
            fileName = "client_settings.json";
        } else {
            fileName = args[0];
        }
        try {
            PiClient testClient = new PiClient(fileName);
            testClient.defaultSetup();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
