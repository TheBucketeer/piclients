package client.SwitchClient;

import java.util.Scanner;

import static java.lang.Thread.sleep;

public class SwitchClientCLI extends SwitchClient {
    private Scanner in;

    private SwitchClientCLI(String clientSettingsFile) throws Exception {
        super(clientSettingsFile);
        this.in = new Scanner(System.in);
    }

    @Override
    protected void run() throws InterruptedException {
        sleep(1000);

        while (!this.stopCondition) {
            this.p("Enter command (quit to exit): ", true);
            String input = this.in.nextLine();
            if (input.length() == 0) {
                continue;
            }
            switch (input) {
                case "in":
                    this.switchInwards();
                    break;
                case "out":
                    this.switchOutwards();
                    break;
                case "update_cc":
                    this.updateChaincode();
                    break;
                case "quit":
                    this.stopCondition = true;
                    break;
                default:
                    this.p("Invalid command", false);
            }
        }
        this.p("Stopped");
    }

    public static void main(String[] args) throws Exception {
        SwitchClientCLI switchClientCLI;

        if (args.length == 0) {
            switchClientCLI = new SwitchClientCLI("client_settings.json");
        } else {
            switchClientCLI = new SwitchClientCLI(args[0]);
        }
        switchClientCLI.run();
    }
}
