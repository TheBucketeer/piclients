package client.LevelCrossingClient;

import java.util.Scanner;

public class LevelCrossingClientCLI extends LevelCrossingClient {
    private Scanner in;

    private LevelCrossingClientCLI(String clientSettingsFile) throws Exception {
        super(clientSettingsFile);
        this.in = new Scanner(System.in);
    }

    @Override
    protected void run() {
        boolean showPrompt = true;
        while (!this.stopCondition) {
            if (showPrompt) {
                this.p("Enter command (quit to exit): ", true);
            }
            String input = this.in.nextLine();

            if (input.length() == 0) {
                showPrompt = false;
                continue;
            } else {
                showPrompt = true;
            }
            int LEDid;
            switch (input) {
                case "close":
                    this.closeForTrains();
                    break;
                case "open":
                    this.openForTrains();
                    break;
                case "update_cc":
                    this.updateChaincode();
                    break;
                case "led_on":
                    this.p("Choose LED to turn on (12-15): ", true);
                    LEDid = this.in.nextInt();
                    this.ledOnSignal(LEDid);
                    break;
                case "led_off":
                    this.p("Choose LED to turn off (12-15): ", true);
                    LEDid = this.in.nextInt();
                    this.ledOffSignal(LEDid);
                    break;
                case "leds_on":
                    this.ledsOnSignal();
                    break;
                case "leds_off":
                    this.ledsOffSignal();
                    break;
                case "loop_leds":
                    this.loopLeds();
                    break;
                case "stop_leds":
                    this.stopLeds();
                    break;
                case "set_servo":
                    while (true) {
                        try {
                            this.p("Choose servo to move (int): ", true);
                            int servoN = this.in.nextInt();
                            this.p("Choose position (float): ", true);
                            float position = this.in.nextFloat();
                            this.setServoPositionHelper(this.servos[servoN], position);
                            break;
                        } catch (Exception e) {
                            this.p(e.getMessage());
                        }
                    }
                    break;
                case "quit":
                    this.stopCondition = true;
                    break;
                default:
                    this.p("Invalid command", false);
            }
        }
        this.p("Stopped");
    }

    public static void main(String[] args) throws Exception {
        LevelCrossingClientCLI levelCrossingCLI;

        if (args.length == 0) {
            levelCrossingCLI = new LevelCrossingClientCLI("client_settings.json");
        } else {
            levelCrossingCLI = new LevelCrossingClientCLI(args[0]);
        }
        levelCrossingCLI.run();
    }
}
