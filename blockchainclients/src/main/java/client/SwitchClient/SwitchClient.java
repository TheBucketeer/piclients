package client.SwitchClient;


import client.PiClient;
import com.pi4j.io.gpio.*;

import static shared.GlobalConfig.CC_FUNCTION.UPDATE_SWITCH_STATE;
import static shared.GlobalConfig.SWITCH_EVENT;
import static shared.assetdata.SwitchConfig.SWITCH_STATE;
import static shared.assetdata.SwitchConfig.SWITCH_STATE.OUTWARDS;

//import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
//import org.hyperledger.fabric.sdk.exception.ProposalException;


public class SwitchClient extends PiClient {
    // Length of the pulse sent to the switch pins, in milliseconds
    private static final long SWITCH_PULSE_DELAY = 100;
    // Minimum interval between handled Switch feedback pin interrupts
    static final long SWITCH_LISTENER_DELAY = 100;

    private final GpioController gpio = GpioFactory.getInstance();
    private GpioPinDigitalOutput insideSwitchPin, outsideSwitchPin;
    volatile SWITCH_STATE currentDirection, lastDirectionSentToCC;
    private volatile long lastEventReadAt;

    SwitchClient(String clientSettingsFile) throws Exception {
        super(clientSettingsFile);
        this.setDefaultCompositeEventListener(SWITCH_EVENT + this.aId);
        this.initChannel();

        this.insideSwitchPin = this.gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00); //red
        this.outsideSwitchPin = this.gpio.provisionDigitalOutputPin(RaspiPin.GPIO_02); //green

        GpioPinDigitalInput insideFeedbackPin = this.gpio.provisionDigitalInputPin(RaspiPin.GPIO_03); //red
        GpioPinDigitalInput outsideFeedbackPin = this.gpio.provisionDigitalInputPin(RaspiPin.GPIO_21); //green

        SwitchFeedbackListener insideListener = new SwitchFeedbackListener(this, SWITCH_STATE.INWARDS, "inside");
        SwitchFeedbackListener outsideListener = new SwitchFeedbackListener(this, SWITCH_STATE.OUTWARDS, "outside");

        insideFeedbackPin.addListener(insideListener);
        outsideFeedbackPin.addListener(outsideListener);

        this.currentDirection = SWITCH_STATE.OUTWARDS;
        this.doSwitch();

        if (insideFeedbackPin.isHigh()) {
            this.updateLocalCurrentDirection(SWITCH_STATE.INWARDS);
        } else if (outsideFeedbackPin.isHigh()) {
            this.updateLocalCurrentDirection(SWITCH_STATE.OUTWARDS);
        }
        SwitchEventListener chaincodeEventHandler = new SwitchEventListener(this);
        Thread thread = new Thread(chaincodeEventHandler);
        thread.start();
    }

    protected void run() throws InterruptedException {
        while (!this.stopCondition) {
            Thread.sleep(100);
            if (this.lastDirectionSentToCC != this.currentDirection) {
                this.updateChaincode();
            }
        }
        this.destroy();
    }

    protected void destroy() {
        this.gpio.shutdown();
    }

    void switchInwards() throws InterruptedException {
        this.insideSwitchPin.high();
        Thread.sleep(SWITCH_PULSE_DELAY);
        this.insideSwitchPin.low();
    }

    void switchOutwards() throws InterruptedException {
        this.outsideSwitchPin.high();
        Thread.sleep(SWITCH_PULSE_DELAY);
        this.outsideSwitchPin.low();
    }

    void doSwitch() throws InterruptedException {
        if (this.currentDirection == SWITCH_STATE.INWARDS) {
            this.switchInwards();
        } else if (this.currentDirection == OUTWARDS) {
            this.switchOutwards();
        } else {
            throw new Error("Oh pleeaasee don't do this to me... (SWITCH_STATE state is not inwards, nor outwards?!)");
        }
    }

    void updateLocalCurrentDirection(SWITCH_STATE state) {
        this.currentDirection = state;
    }

    void updateChaincode() {
        SWITCH_STATE toSend = this.currentDirection;
        this.invokeCC(UPDATE_SWITCH_STATE, this.aId, toSend.toString());
        this.lastDirectionSentToCC = toSend;
    }

    long getLastEventReadAt() {
        return lastEventReadAt;
    }

    void setLastEventReadAt(long lastEventReadAt) {
        this.lastEventReadAt = lastEventReadAt;
    }

    public static void main(String[] args) throws Exception {
        SwitchClient switchClient;
        if (args.length == 0) {
            switchClient = new SwitchClient("client_settings.json");
        } else {
            switchClient = new SwitchClient(args[0]);
        }
        switchClient.run();
    }
}
