package client;

import org.json.JSONObject;

import static java.lang.Thread.sleep;
import static shared.GlobalConfig.KILL_EVENT;

public abstract class PiClientEventListenerBase implements Runnable {
    protected abstract PiClient getClient();

    protected abstract void handleEvent(JSONObject event) throws InterruptedException;

    protected void p(Object o) {
        System.out.println("[" + this.getClass().getName() + "]: " + o);
    }

    @Override
    public void run() {
        try {
            sleep(1000);
            while (!this.getClient().stopCondition) {
                if (this.getClient().jsonChaincodeEvents.size() == 0) {
                    sleep(100);
                } else {
                    JSONObject j = this.getClient().jsonChaincodeEvents.poll();
                    if (j == null) {
                        throw new RuntimeException("jsonChaincodeEvent was null?!");
                    }
                    this.getClient().eventLog("Event received: " + j.toString());
                    if (j.getString("eventName").equals(KILL_EVENT)) {
                        this.getClient().stopCondition = true;
                    } else {
                        this.handleEvent(j);
                    }
                }
            }
            this.p("Stopped.");
        } catch (InterruptedException e) {
            this.p(e.getMessage());
            this.getClient().stopCondition = true;
        }
    }
}
