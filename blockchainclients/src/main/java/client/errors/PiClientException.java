package client.errors;

class PiClientException extends Exception {

    PiClientException() {
        super();
    }

    public PiClientException(Throwable cause) {
        super(cause);
    }

    PiClientException(String message) {
        super(message);
    }

    public PiClientException(String message, Throwable cause) {
        super(message, cause);
    }
}
