package client.SignalClient;

import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

class SignalFeedbackListener implements GpioPinListenerDigital {
    private SignalClient mom;
    private String name;

    SignalFeedbackListener(SignalClient mom, String name) {
        this.mom = mom;
        this.name = name;
    }

    private void p(Object o) {
        System.out.println("[SignalFeedbackListener]: " + o);
    }

    public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
        this.p("The " + this.name + " signal light is now " + (event.getState() == PinState.HIGH ? "on" : "off"));
        this.mom.determineCurrentAspect();
    }
}
