package client.TrainClient;

import client.PiClient;
import client.errors.CommandQueueException;
import org.hyperledger.fabric.sdk.exception.BaseException;
import org.json.JSONObject;
import shared.assetdata.TrainConfig.TRAIN_DIRECTION;
import shared.assetdata.TrainConfig.TRAIN_SPEED;
import shared.errors.InvalidTrainSpeedException;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ConcurrentLinkedQueue;

import static java.lang.Thread.sleep;
import static shared.GlobalConfig.CC_FUNCTION.*;
import static shared.GlobalConfig.TRAIN_CONTROL_EVENT;
import static shared.assetdata.TrainConfig.TRAIN_DIRECTION.FORWARDS;

//import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
//import org.hyperledger.fabric.sdk.exception.ProposalException;

public class TrainClient extends PiClient {
    private static final String JMRI_IP = "10.0.0.1";
    private static final int JMRI_PORT = 2056;

    private int address;
    volatile boolean talkerHasStopped = false;
    volatile TRAIN_SPEED speed = TRAIN_SPEED.fromDouble(0.0);
    volatile TRAIN_DIRECTION direction = FORWARDS;
    ConcurrentLinkedQueue<JSONObject> commandQueue = new ConcurrentLinkedQueue<>();


    TrainClient(String clientSettingsFile) throws IOException, InstantiationException, InvocationTargetException, NoSuchMethodException, BaseException, IllegalAccessException, ClassNotFoundException, InvalidTrainSpeedException {
        super(clientSettingsFile);
        this.setDefaultCompositeEventListener(TRAIN_CONTROL_EVENT + this.aId);
        this.initChannel();
        this.address = this.jo.getInt("train_address");
        new TrainClientTalker(JMRI_IP, JMRI_PORT, this).start();
        TrainEventListener chaincodeEventHandler = new TrainEventListener(this);
        Thread thread = new Thread(chaincodeEventHandler);
        thread.start();
    }

    @Override
    protected void destroy() {
        super.destroy();
        while (!this.talkerHasStopped) {
            try {
                sleep(100);
            } catch (InterruptedException e) {
                this.p(e.getMessage());
            }
        }
        this.p("Thread has also stopped, quitting for good");
    }

    private JSONObject getCommandTemplate() {
        JSONObject command = new JSONObject();
        JSONObject data = new JSONObject();

        data.put("throttle", this.aId);
        command.put("type", "throttle");
        command.put("data", data);
        return command;
    }

    private JSONObject createInitCommand() {
        JSONObject initCommand = getCommandTemplate();
        initCommand.getJSONObject("data").put("address", this.address);
        this.p("Created init command " + initCommand.toString());
        return initCommand;
    }

    private JSONObject createReverseCommand() {
        JSONObject reverseCommand = getCommandTemplate();
        reverseCommand.getJSONObject("data").put("forward", String.valueOf(this.direction.toBool()));
        this.p("Created reverse command " + reverseCommand.toString());
        return reverseCommand;
    }

    private JSONObject createSpeedCommand() {
        JSONObject speedCommand = getCommandTemplate();
        speedCommand.getJSONObject("data").put("speed", this.speed.toDouble());
        this.p("Created speed command " + speedCommand.toString());
        return speedCommand;
    }

    private void sendCommand(JSONObject command) throws CommandQueueException {
        if (this.commandQueue.add(command)) {
            this.p("Successfully added command to the commandQueue: " + command.toString());
        } else {
            this.p("Failed to add command to the commandQueue: " + command.toString());
            throw new CommandQueueException();
        }
    }

    // Applies the speed change and then waits a second before returning,
    // if the new speed was a full stop, to ensure that the train has stopped fully before returning
    void changeSpeed(TRAIN_SPEED newSpeed) throws CommandQueueException, InterruptedException {
        this.speed = newSpeed;
        this.sendCommand(this.createSpeedCommand());
        if (newSpeed.equals(TRAIN_SPEED.getStopSpeed())) {
            sleep(1000);
        }
    }

    void updateDirectionToBC() {
        this.invokeCC(UPDATE_TRAIN_DIRECTION, this.aId, this.direction.toString());
    }

    void updateSpeedToBC() {
        this.invokeCC(UPDATE_TRAIN_SPEED, this.aId, this.speed.toString());
    }


    // First stops the train, then turns it around
    void turnAround() throws CommandQueueException, InterruptedException {
        TRAIN_SPEED oldSpeed = this.speed;
        this.changeSpeed(TRAIN_SPEED.getStopSpeed());
        this.sendCommand(this.createReverseCommand());
        this.changeSpeed(oldSpeed);
    }

    void initTrain() {
        try {
            this.sendCommand(this.createInitCommand());
            this.invokeCC(TRAIN_INIT_CONFIRM, this.aId);
        } catch (CommandQueueException e) {
            this.p(e.getMessage());
            e.printStackTrace();
        }
    }

    static public void main(String[] args) throws Exception {
        TrainClient trainClient;
        if (args.length == 0) {
            trainClient = new TrainClient("client_settings.json");
        } else {
            trainClient = new TrainClient(args[0]);
        }
        trainClient.run();
    }
}
