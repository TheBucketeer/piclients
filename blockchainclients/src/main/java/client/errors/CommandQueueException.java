package client.errors;


public class CommandQueueException extends PiClientException {
    public CommandQueueException() {
        super("Failed to add command to queue");
    }
}
