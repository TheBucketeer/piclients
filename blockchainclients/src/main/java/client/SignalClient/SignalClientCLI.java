package client.SignalClient;


import shared.assetdata.SignalConfig.SIGNAL_ASPECT;

import java.util.Scanner;

import static java.lang.Thread.sleep;

public class SignalClientCLI extends SignalClient {
    private Scanner in;
    private boolean isTest = false;

    private SignalClientCLI(String clientSettingsFile) throws Exception {
        super(clientSettingsFile);
        this.in = new Scanner(System.in);
    }

    private SignalClientCLI(String clientSettingsFile, boolean isTest) throws Exception {
        this(clientSettingsFile);
        this.isTest = isTest;
    }

    protected void run() throws InterruptedException {
        sleep(1000);

        if (this.isTest) {
            while (!this.stopCondition) {
                this.updateAspect(SIGNAL_ASPECT.RED);
                sleep(1000);
                this.updateAspect(SIGNAL_ASPECT.GREEN);
                sleep(1000);
                this.updateAspect(SIGNAL_ASPECT.RED_GREEN);
                sleep(1000);
                this.updateAspect(SIGNAL_ASPECT.OFF);
                sleep(1000);
            }
        } else {
            while (!this.stopCondition) {
                this.p("Enter command (quit to exit): ", true);
                String input = this.in.nextLine();
                String pinName;
                if (input.length() == 0) {
                    continue;
                }
                switch (input) {
                    case "red":
                        this.p("Changing aspect to " + input);
                        this.updateAspect(SIGNAL_ASPECT.RED);
                        break;
                    case "green":
                        this.p("Changing aspect to " + input);
                        this.updateAspect(SIGNAL_ASPECT.GREEN);
                        break;
                    case "yellow":
                        this.p("Changing aspect to " + input);
                        this.updateAspect(SIGNAL_ASPECT.RED_GREEN);
                        break;
                    case "update_cc":
                        this.updateChaincode();
                        break;
                    case "quit":
                        this.stopCondition = true;
                        break;
                    default:
                        this.p("Invalid command", false);
                }
            }
        }
    }

    public static void main(String[] args) throws Exception {
        SignalClientCLI signalClient;
        if (args.length == 0) {
            signalClient = new SignalClientCLI("client_settings.json");
        } else {
            if (args.length == 2) {
                signalClient = new SignalClientCLI(args[0], true);
            } else {
                signalClient = new SignalClientCLI(args[0]);
            }
        }
        signalClient.run();
    }
}
