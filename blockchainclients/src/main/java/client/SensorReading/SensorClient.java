package client.SensorReading;


import client.PiClient;
import com.pi4j.io.gpio.*;

import static shared.GlobalConfig.CC_FUNCTION.SENSOR_TRIGGER;

//import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
//import org.hyperledger.fabric.sdk.exception.ProposalException;

public class SensorClient extends PiClient {
    private GpioPinDigitalOutput ledPin;
    volatile boolean triggered = false;


    private SensorClient(String clientSettingsFile) throws Exception {
        super(clientSettingsFile);
        this.initChannel();
        GpioController gpio = GpioFactory.getInstance();
        GpioPinDigitalInput sensor = gpio.provisionDigitalInputPin(RaspiPin.GPIO_22);
        this.ledPin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_28);
        SensorListener listener = new SensorListener(this);
        sensor.addListener(listener);
        this.ledPin.low();
        SensorEventListener chaincodeEventHandler = new SensorEventListener(this);
        Thread ccEventThread = new Thread(chaincodeEventHandler);
        ccEventThread.start();
    }

    @Override
    protected void run() {
        while (!this.stopCondition) {
            if (this.triggered) {
                this.triggered = false;
                this.ledPin.high();
                this.p(this.invokeCC(SENSOR_TRIGGER, this.aId));
            } else {
                this.ledPin.low();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        SensorClient sensorClient;
        if (args.length == 0) {
            sensorClient = new SensorClient("client_settings.json");
        } else {
            sensorClient = new SensorClient(args[0]);
        }
        sensorClient.run();
    }
}
