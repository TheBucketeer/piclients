package client.LevelCrossingClient;

class LevelCrossingClientXMasLightHandler extends Thread {
    private LevelCrossingClient levelCrossingClient;
    private boolean status = false;
    private int loopIntervalMs = 5000;

    LevelCrossingClientXMasLightHandler(LevelCrossingClient levelCrossingClient) {
        this.levelCrossingClient = levelCrossingClient;
    }

    @Override
    public void run() {
        try {
            while (!this.levelCrossingClient.stopCondition) {
                if (this.status) {
                    this.levelCrossingClient.halfLedsOnSignal(true);
                    Thread.sleep(this.loopIntervalMs);
                    this.levelCrossingClient.halfLedsOnSignal(false);
                    Thread.sleep(this.loopIntervalMs);
                } else {
                    this.levelCrossingClient.ledsOffSignal();
                }
            }
        } catch (InterruptedException e) {
            // Die quietly
        }
        this.levelCrossingClient.ledsOffSignal();
    }

    void setStatus(boolean on) {
        this.status = on;
    }

    void setLoopIntervalMs(int loopIntervalMs) {
        this.loopIntervalMs = loopIntervalMs;
    }
}