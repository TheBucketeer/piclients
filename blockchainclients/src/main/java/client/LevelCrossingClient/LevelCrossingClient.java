package client.LevelCrossingClient;


import client.PiClient;
import com.pi4j.component.servo.Servo;
import com.pi4j.component.servo.impl.GenericServo;
import com.pi4j.component.servo.impl.PCA9685GpioServoProvider;
import com.pi4j.gpio.extension.pca.PCA9685GpioProvider;
import com.pi4j.gpio.extension.pca.PCA9685Pin;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CFactory;
import shared.assetdata.LevelCrossingConfig.LC_STATE;

import java.math.BigDecimal;

import static com.pi4j.gpio.extension.pca.PCA9685Pin.ALL;
import static shared.GlobalConfig.CC_FUNCTION.UPDATE_LEVEL_CROSSING_STATE;
import static shared.GlobalConfig.LEVEL_CROSSING_EVENT;
import static shared.assetdata.LevelCrossingConfig.LC_STATE.CLOSED_FOR_TRAINS;
import static shared.assetdata.LevelCrossingConfig.LC_STATE.OPEN_FOR_TRAINS;

//import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
//import org.hyperledger.fabric.sdk.exception.ProposalException;

public class LevelCrossingClient extends PiClient {
    private final PCA9685GpioProvider gpioProvider;
    private final PCA9685GpioServoProvider gpioServoProvider;

    Servo[] servos;

    private LC_STATE state;
    private static final int NUMBER_OF_BARRIERS = 2;
    private static final int OPEN_POSITION = 0;
    private static final float CLOSED_POSITION = 210;
    static final int[] LED_PIN_NUMBERS = new int[]{8, 9, 10, 11, 12, 13, 14, 15};
    private static final int LED_LOOP_INTERVAL = 250;
    private LevelCrossingClientXMasLightHandler XMasLightHandler;

    LevelCrossingClient(String clientSettingsFile) throws Exception {
        super(clientSettingsFile);
        this.gpioProvider = new PCA9685GpioProvider(I2CFactory.getInstance(I2CBus.BUS_1), 0x40, new BigDecimal("50.0"));
        GpioController gpio = GpioFactory.getInstance();
        for (int i = 0; i < ALL.length; i++) {
            gpio.provisionPwmOutputPin(this.gpioProvider, PCA9685Pin.ALL[i], "Servo " + i);
        }
        this.gpioProvider.reset();
        this.gpioServoProvider = new PCA9685GpioServoProvider(gpioProvider);
        this.servos = new Servo[NUMBER_OF_BARRIERS];
        for (int i = 0; i < NUMBER_OF_BARRIERS; i++) {
            this.servos[i] = new GenericServo(gpioServoProvider.getServoDriver(PCA9685Pin.ALL[i]), "Barrier " + i);
        }

        this.p("Initialised I/O");
        this.setDefaultCompositeEventListener(LEVEL_CROSSING_EVENT + this.aId);
        this.initChannel();
        this.p("Initialised channel");
        LevelCrossingEventListener chaincodeEventHandler = new LevelCrossingEventListener(this);
        Thread ccEventThread = new Thread(chaincodeEventHandler);
        ccEventThread.start();
        this.p("Started chaincodeEventHandler");
        this.XMasLightHandler = new LevelCrossingClientXMasLightHandler(this);
        Thread XMasLightsThread = new Thread(this.XMasLightHandler);
        XMasLightsThread.start();
        this.XMasLightHandler.setLoopIntervalMs(LED_LOOP_INTERVAL);
        this.p("Started christmas light thread");

        this.state = OPEN_FOR_TRAINS;
        this.closeForTrains();
    }

    void updateState(LC_STATE newState) {
        if (this.state != newState) {
            this.p("Will change state to: " + newState.getName());

            // Send signal to output pin
            if (newState == OPEN_FOR_TRAINS) {
                this.openForTrains();
            } else if (newState == CLOSED_FOR_TRAINS) {
                this.closeForTrains();
            } else {
                this.p("Unexpected level crossing state: " + this.state.getName());
            }
            // update chaincode
            this.updateChaincode();
        }
    }

    void updateChaincode() {
        this.invokeCC(UPDATE_LEVEL_CROSSING_STATE, this.aId, this.state.toString());
    }

    void closeForTrains() {
        for (Servo servo : this.servos) {
            this.p("Opening " + servo.getName());
            this.setServoPositionHelper(servo, OPEN_POSITION);
        }
        this.XMasLightHandler.setStatus(false);
        this.state = CLOSED_FOR_TRAINS;
    }

    void openForTrains() {
        for (Servo servo : this.servos) {
            this.p("Closing " + servo.getName());
            this.setServoPositionHelper(servo, CLOSED_POSITION);
        }
        this.XMasLightHandler.setStatus(true);
        this.state = OPEN_FOR_TRAINS;
    }

    void loopLeds() {
        this.XMasLightHandler.setStatus(true);
    }

    void stopLeds() {
        this.XMasLightHandler.setStatus(false);
    }

    // Because the library was completely broken, we have to do this manually
    void setServoPositionHelper(Servo servo, float position) {
        int pulseWidth = ((int) position + 100) * 5 + 1000;
        servo.getServoDriver().setServoPulseWidth(pulseWidth);
    }

    void ledsOnSignal() {
        for (int LEDid : LED_PIN_NUMBERS) {
            this.ledOnSignal(LEDid);
        }
    }

    void ledOnSignal(int nLED) {
        this.gpioProvider.setAlwaysOff(ALL[nLED]);
    }

    void ledsOffSignal() {
        for (int LEDid : LED_PIN_NUMBERS) {
            this.ledOffSignal(LEDid);
        }
    }

    void ledOffSignal(int nLED) {
        this.gpioProvider.setAlwaysOn(ALL[nLED]);
    }

    void halfLedsOnSignal(boolean firstOn) {
        boolean on = firstOn;
        for (int LEDid : LED_PIN_NUMBERS) {
            if (on) {
                this.ledOnSignal(LEDid);
            } else {
                this.ledOffSignal(LEDid);
            }
            on = !on;
        }
    }

    public static void main(String[] args) throws Exception {
        LevelCrossingClient levelCrossingClient;
        if (args.length == 0) {
            levelCrossingClient = new LevelCrossingClient("client_settings.json");
        } else {
            levelCrossingClient = new LevelCrossingClient(args[0]);
        }
        levelCrossingClient.run();
    }
}
