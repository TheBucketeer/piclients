package client.SignalClient;


import client.PiClient;
import com.pi4j.io.gpio.*;
import shared.assetdata.SignalConfig.SIGNAL_ASPECT;

import static java.lang.Thread.sleep;
import static shared.GlobalConfig.CC_FUNCTION.UPDATE_SIGNAL_ASPECT;
import static shared.GlobalConfig.SIGNAL_EVENT;

//import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
//import org.hyperledger.fabric.sdk.exception.ProposalException;

public class SignalClient extends PiClient {
    private GpioPinDigitalOutput redOutputPin, greenOutputPin;
    private GpioPinDigitalInput redFeedbackPin, greenFeedbackPin;
    private SIGNAL_ASPECT intendedAspect, actualAspect, lastAspectSentToCC;

    SignalClient(String clientSettingsFile) throws Exception {
        super(clientSettingsFile);
        GpioController gpio = GpioFactory.getInstance();
        this.redOutputPin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00);
        this.greenOutputPin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_02);

        this.redFeedbackPin = gpio.provisionDigitalInputPin(RaspiPin.GPIO_03);
        this.greenFeedbackPin = gpio.provisionDigitalInputPin(RaspiPin.GPIO_21);

        SignalFeedbackListener redFeedbackListener = new SignalFeedbackListener(this, "red");
        SignalFeedbackListener greenFeedbackListener = new SignalFeedbackListener(this, "green");

        this.redFeedbackPin.addListener(redFeedbackListener);
        this.greenFeedbackPin.addListener(greenFeedbackListener);

        this.intendedAspect = SIGNAL_ASPECT.RED;
        this.setSignal();
        this.setDefaultCompositeEventListener(SIGNAL_EVENT + this.aId);
        this.initChannel();

        SignalEventListener chaincodeEventHandler = new SignalEventListener(this);
        new Thread(chaincodeEventHandler).start();
    }

    protected void run() throws InterruptedException {
        this.updateAspect(SIGNAL_ASPECT.RED);
        while (!this.stopCondition) {
            if (this.actualAspect != this.lastAspectSentToCC) {
                this.updateChaincode();
            }
            sleep(100);
        }
    }

    void updateAspect(SIGNAL_ASPECT newAspect) {
        if (this.intendedAspect != newAspect) {
            this.p("Changing aspect to " + newAspect.getName());
            this.intendedAspect = newAspect;
            this.setSignal();
        }
    }

    void determineCurrentAspect() {
        if (this.greenFeedbackPin.isHigh() && this.redFeedbackPin.isHigh()) {
            this.actualAspect = SIGNAL_ASPECT.RED_GREEN;
        } else if (this.greenFeedbackPin.isHigh() && !this.redFeedbackPin.isHigh()) {
            this.actualAspect = SIGNAL_ASPECT.GREEN;
        } else if (!this.greenFeedbackPin.isHigh() && this.redFeedbackPin.isHigh()) {
            this.actualAspect = SIGNAL_ASPECT.RED;
        } else {
            this.actualAspect = SIGNAL_ASPECT.OFF;
        }
    }

    private void setSignal() {
        switch (this.intendedAspect) {
            case RED:
                this.redOutputPin.high();
                this.greenOutputPin.low();
                break;
            case RED_GREEN:
                this.redOutputPin.high();
                this.greenOutputPin.high();
                break;
            case GREEN:
                this.redOutputPin.low();
                this.greenOutputPin.high();
                break;
            case OFF:
                this.redOutputPin.low();
                this.greenOutputPin.low();
                break;
            default:
                this.p("UNDEFINED ASPECT FOUND: " + this.intendedAspect.getName());
                this.redOutputPin.high();
                this.greenOutputPin.low();
                break;
        }
    }

    void updateChaincode() {
        SIGNAL_ASPECT toSend = this.actualAspect;
        this.invokeCC(UPDATE_SIGNAL_ASPECT, this.aId, String.valueOf(toSend.toInt()));
        this.lastAspectSentToCC = toSend;
    }

    public static void main(String[] args) throws Exception {
        SignalClient signalClient;
        if (args.length == 0) {
            signalClient = new SignalClient("client_settings.json");
        } else {
            signalClient = new SignalClient(args[0]);
        }
        signalClient.run();
    }
}
