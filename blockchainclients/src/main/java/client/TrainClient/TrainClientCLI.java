package client.TrainClient;


import client.errors.CommandQueueException;
import shared.assetdata.TrainConfig.TRAIN_DIRECTION;
import shared.assetdata.TrainConfig.TRAIN_SPEED;
import shared.errors.InvalidTrainSpeedException;

import java.util.Scanner;

import static java.lang.Thread.sleep;
import static shared.assetdata.TrainConfig.TRAIN_DIRECTION.FORWARDS;


public class TrainClientCLI extends TrainClient {
    private Scanner in;
    private TRAIN_DIRECTION direction = FORWARDS;


    private TrainClientCLI(String clientSettingsFile) throws Exception {
        super(clientSettingsFile);
        this.in = new Scanner(System.in);
    }

    private void help() {
        this.p("This program accepts the following commands:\n\n" +
                "\t+ a: accelerate, increase speed by 5%\n" +
                "\t+ d: decelerate, decrease speed by 5%\n" +
                "\t+ stop: emergency stop, stops the train immediately\n" +
                "\t+ reverse: let the train drive in opposite direction\n" +
                "\t+ quit: stop the train and exit this program\n" +
                "\t+ help: show this help text\n\n", false);
    }

    @Override
    protected void run() throws CommandQueueException, InterruptedException {
        sleep(1000);

        while (!this.stopCondition) {
            this.p("Enter command (quit to exit): ", true);
            String input = this.in.nextLine();
            TRAIN_SPEED oldSpeed, newSpeed;
            try {
                switch (input) {
                    case "init":
                        this.initTrain();
                        break;
                    case "reverse":
                        this.direction = this.direction.getOpposite();
                        this.turnAround();
                        break;
                    case "a":
                        oldSpeed = this.speed;
                        if (this.speed.equals(TRAIN_SPEED.getStopSpeed())) {
                            newSpeed = TRAIN_SPEED.fromDouble(0.05);
                        } else {
                            newSpeed = TRAIN_SPEED.fromDouble(this.speed.toDouble() + 0.05);
                        }
                        this.changeSpeed(newSpeed);
                        this.p("Previous speed was " + oldSpeed.toDouble() + ", new speed is " + newSpeed.toDouble() + ", actual speed is " + this.speed.toDouble());
                        break;
                    case "d":
                        oldSpeed = this.speed;
                        if (this.speed.toDouble() == 0) {
                            newSpeed = TRAIN_SPEED.getStopSpeed();
                        } else {
                            newSpeed = TRAIN_SPEED.fromDouble(this.speed.toDouble() - 0.05);
                        }
                        this.changeSpeed(newSpeed);
                        this.p("Previous speed was " + oldSpeed.toDouble() + ", new speed is " + newSpeed.toDouble() + ", actual speed is " + this.speed.toDouble());
                        break;
                    case "set_speed":
                        double speed = this.in.nextDouble();
                        this.changeSpeed(TRAIN_SPEED.fromDouble(speed));
                        break;
                    case "stop":
                        this.changeSpeed(TRAIN_SPEED.getStopSpeed());
                        break;
                    case "update_cc":
                        this.updateDirectionToBC();
                        this.updateSpeedToBC();
                        break;
                    case "quit":
                        this.stopCondition = true;
                        this.p("Exiting", false);
                        break;
                    case "help":
                        help();
                        break;
                    default:
                        this.p("Invalid command: " + input, false);
                        help();
                }
            } catch (InvalidTrainSpeedException e) {
                this.p(e.getMessage());
            }
        }
    }

    public static void main(String[] args) throws Exception {
        TrainClientCLI trainClientCLI;

        if (args.length == 0) {
            trainClientCLI = new TrainClientCLI("client_settings.json");
        } else {
            trainClientCLI = new TrainClientCLI(args[0]);
        }

        trainClientCLI.run();
    }
}
