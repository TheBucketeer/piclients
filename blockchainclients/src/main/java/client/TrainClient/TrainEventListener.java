package client.TrainClient;

import client.PiClient;
import client.PiClientEventListenerBase;
import client.errors.CommandQueueException;
import org.json.JSONObject;
import shared.assetdata.TrainConfig.TRAIN_CMD_TYPE;
import shared.assetdata.TrainConfig.TRAIN_DIRECTION;
import shared.assetdata.TrainConfig.TRAIN_SPEED;
import shared.errors.InvalidTrainCommandException;
import shared.errors.InvalidTrainSpeedException;

import static shared.assetdata.TrainConfig.TRAIN_CMD_TYPE.strToTrainCommand;

class TrainEventListener extends PiClientEventListenerBase {
    private TrainClient dadsies;

    TrainEventListener(TrainClient daddy) {
        super();
        this.dadsies = daddy;
    }

    public void handleEvent(JSONObject event) throws InterruptedException {
        this.p("Command received: " + event.getString("type"));
        try {
            TRAIN_CMD_TYPE cmdType = strToTrainCommand(event.getString("type"));
            switch (cmdType) {
                case INIT:
                    this.dadsies.initTrain();
                    break;
                case SET_SPEED:
                    this.dadsies.changeSpeed(TRAIN_SPEED.fromDouble(event.getDouble("speed")));
                    this.dadsies.updateSpeedToBC();
                    break;
                case SET_DIRECTION:
                    TRAIN_DIRECTION newDirection = TRAIN_DIRECTION.fromBool(event.getBoolean("direction"));
                    if (!newDirection.equals(this.dadsies.direction)) {
                        this.dadsies.direction = this.dadsies.direction.getOpposite();
                        this.dadsies.turnAround();
                    }
                    this.dadsies.updateDirectionToBC();
                    break;
                default:
                    this.p("ERROR! Command not unknown, not known? " + event.getString("type"));
            }
        } catch (CommandQueueException | InvalidTrainSpeedException | InvalidTrainCommandException e) {
            this.p(e.getMessage());
        }
    }

    public PiClient getClient() {
        return this.dadsies;
    }
}