package client.SensorReading;

import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

class SensorListener implements GpioPinListenerDigital {
    private SensorClient mumsies;

    SensorListener(SensorClient mums) {
        this.mumsies = mums;
    }

    private void p(Object o) {
        System.out.println("[SensorListener]: " + o);
    }

    @Override
    public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
        this.p("SENSOR TRIGGERED! (event = " + event.toString() + ")");
        this.mumsies.triggered = true;
    }
}
