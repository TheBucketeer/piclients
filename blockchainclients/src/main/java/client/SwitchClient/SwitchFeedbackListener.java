package client.SwitchClient;

import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import shared.assetdata.SwitchConfig;

import static client.SwitchClient.SwitchClient.SWITCH_LISTENER_DELAY;


class SwitchFeedbackListener implements GpioPinListenerDigital {
    private SwitchClient mom;
    private SwitchConfig.SWITCH_STATE direction;
    private String name;

    SwitchFeedbackListener(SwitchClient mom, SwitchConfig.SWITCH_STATE direction, String name) {
        this.mom = mom;
        this.name = name;
        this.direction = direction;
    }

    private void p(Object o) {
        this.mom.p(o);
    }

    public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
        if (event.getState() == PinState.HIGH) {
            this.p(this.name + " became high");
            long eventReadAt = System.currentTimeMillis();
            if (eventReadAt - this.mom.getLastEventReadAt() > SWITCH_LISTENER_DELAY) {
                this.mom.setLastEventReadAt(eventReadAt);
                this.p("Switch changed to " + this.name + " wire!");
                this.mom.updateLocalCurrentDirection(this.direction);
                if (this.mom.lastDirectionSentToCC != this.mom.currentDirection) {
                    this.mom.updateChaincode();
                }
            } else {
                this.p("Ignored change to " + this.name + " wire!");
            }
        } else {
            this.p(this.name + " became low");
        }
    }
}
