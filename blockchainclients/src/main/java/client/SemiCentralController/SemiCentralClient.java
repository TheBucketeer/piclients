package client.SemiCentralController;


import client.PiClient;
import shared.assetdata.SignalConfig.SIGNAL_ASPECT;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

import static shared.GlobalConfig.CC_FUNCTION;
import static shared.GlobalConfig.CC_FUNCTION.*;
import static shared.GlobalConfig.SIGNAL_AID_PREFIX;

//import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
//import org.hyperledger.fabric.sdk.exception.ProposalException;

public class SemiCentralClient extends PiClient {
    private String promptStr;
    private boolean wasInPrompt;
    private String testName = null;
    private int testDelay = -1;

    private LinkedList<String> compositeKeys = new LinkedList<>();

    private SemiCentralClient(String clientSettingsFile) throws Exception {
        super(clientSettingsFile);
        this.setDefaultAnyCompositeEventListener();
        this.initChannel();
        this.stopCondition = false;
        SemiCentralEventListener chaincodeEventHandler = new SemiCentralEventListener(this);
        Thread thread = new Thread(chaincodeEventHandler);
        thread.start();
    }

    private SemiCentralClient(String clientSettingsFile, String testName, int testDelay) throws Exception {
        this(clientSettingsFile);
        this.testName = testName;
        this.testDelay = testDelay;
    }

    public void p(Object o) {
        this.p(o, false);
    }

    protected void p(Object o, boolean isPrompt) {
        synchronized (this) {
            if (isPrompt && this.wasInPrompt && ("" + o).equals(this.promptStr)) {
                return;
            }
            if (this.wasInPrompt) {
                System.out.println();
            }
            this.promptStr = isPrompt ? "" + o : "";
            super.p(o, isPrompt);
            if (!isPrompt && this.wasInPrompt) {
                super.p(this.promptStr, true);
            }
            this.wasInPrompt = isPrompt;
        }
    }

    private void readTest(int delay) throws InterruptedException {
        LinkedList<CC_FUNCTION> testFunctions = new LinkedList<>(
                Arrays.asList(
                        QUERY_SECTION_RESERVEE,
                        QUERY_SECTION_RESERVEE,
                        QUERY_SECTION_RESERVEE,
                        QUERY_TRAIN_DESTINATION,
                        QUERY_TRAIN_DESTINATION,
                        QUERY_TRAIN_POSITION,
                        QUERY_TRAIN_POSITION,
                        QUERY_TRAIN_DESTINATION,
                        QUERY_TRAIN_DESTINATION,
                        QUERY_SECTION_RESERVEE,
                        QUERY_SECTION_RESERVEE,
                        QUERY_SECTION_RESERVEE,
                        QUERY_SECTION_RESERVEE
                )
        );
        String[][] testArgs = new String[][]{
                new String[]{"Sc014"},
                new String[]{"Sc013"},
                new String[]{"Sc012"},
                new String[]{"Tr000"},
                new String[]{"Tr001"},
                new String[]{"Tr000"},
                new String[]{"Tr001"},
                new String[]{"Tr000"},
                new String[]{"Tr001"},
                new String[]{"Sc000"},
                new String[]{"Sc001"},
                new String[]{"Sc003"},
                new String[]{"Sc004"}
        };
        this.performanceTest(testFunctions, delay, testArgs);
    }

    private void writeTest(int delay) throws InterruptedException {
        LinkedList<CC_FUNCTION> testFunctions = new LinkedList<>(
                Arrays.asList(
                        SET_TRAIN_DESTINATION,
                        SET_TRAIN_DESTINATION,
                        SET_TRAIN_DESTINATION,
                        SET_TRAIN_DESTINATION,
                        SET_TRAIN_DESTINATION,
                        SET_TRAIN_DESTINATION,
                        SET_TRAIN_DESTINATION,
                        SET_TRAIN_DESTINATION,
                        SET_TRAIN_DESTINATION,
                        SET_TRAIN_DESTINATION,
                        SET_TRAIN_DESTINATION,
                        SET_TRAIN_DESTINATION,
                        SET_TRAIN_DESTINATION
                )
        );
        String[][] testArgs = new String[][]{
                new String[]{"Tr000", "Sc014"},
                new String[]{"Tr000", "Sc013"},
                new String[]{"Tr000", "Sc012"},
                new String[]{"Tr000", "Sc011"},
                new String[]{"Tr000", "Sc010"},
                new String[]{"Tr000", "Sc009"},
                new String[]{"Tr000", "Sc008"},
                new String[]{"Tr000", "Sc007"},
                new String[]{"Tr000", "Sc006"},
                new String[]{"Tr000", "Sc005"},
                new String[]{"Tr000", "Sc004"},
                new String[]{"Tr000", "Sc003"},
                new String[]{"Tr000", "Sc002"}
        };
        this.performanceTest(testFunctions, delay, testArgs);
    }

    private void performanceTest(LinkedList<CC_FUNCTION> testFunctions, int delay, String[]... testArgs) throws InterruptedException {
        for (int i = 0; i < testFunctions.size(); i++) {
            if (i < 3) {
                this.invokeCC(testFunctions.get(i), testArgs[i]);
            } else {
                this.p("Run " + i);
                this.p(this.invokeCC(testFunctions.get(i), testArgs[i]));
            }
            Thread.sleep(delay * 1000);
        }
        Thread.sleep(10000);
    }

    private void signalSetTest() {
        for (int i = 0; i < 16; i++) {
            this.invokeCC(SET_SIGNAL_ASPECT, String.format("%s%03d", SIGNAL_AID_PREFIX, i), SIGNAL_ASPECT.RED_GREEN.toString());
        }
    }

    private void signalUpdateTest() {
        for (int i = 0; i < 16; i++) {
            this.invokeCC(UPDATE_SIGNAL_ASPECT, String.format("%s%03d", SIGNAL_AID_PREFIX, i), SIGNAL_ASPECT.RED_GREEN.toString());
        }
    }

    private int testvarTest(int x, int n) {
        this.compositeKeys.clear();
        for (int i = x; i < x + n; i++) {
            String r = this.invokeCC(SET_TEST_VAR, "l", String.valueOf(i));
            this.p(r);
            try {
                Thread.sleep(0);
            } catch (InterruptedException ignored) {

            }
        }
        return x + n;
    }

    @Override
    protected void run() throws InterruptedException {
        int x = 0;
        if (this.testName != null) {
            this.logFileName = this.testName + ".txt";
            this.eventLogFileName = this.testName + "_events.txt";
            this.readTest(this.testDelay);
            this.writeTest(this.testDelay);
            this.stopCondition = true;
        } else {
            Scanner scanner = new Scanner(System.in);
            String cmd;
            start:
            while (!this.stopCondition) {
                this.p("Enter command: ", true);
                try {
                    while (System.in.available() == 0) {
                        Thread.sleep(1000);
                        if (this.stopCondition) {
                            return;
                        }
                    }
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
                try {
                    cmd = scanner.nextLine();
                    int delay;
                    switch (cmd) {
                        case "read_test":
                            this.p("Enter delay in seconds: ", true);
                            delay = scanner.nextInt();
                            this.readTest(delay);
                            break;
                        case "write_test":
                            this.p("Enter delay in seconds: ", true);
                            delay = scanner.nextInt();
                            this.writeTest(delay);
                            break;
                        case "signal_set_test":
                            this.signalSetTest();
                            break;
                        case "signal_update_test":
                            this.signalUpdateTest();
                            break;
                        case "t":
                            x = this.testvarTest(x, 10);
                            break;
                        case "q":
                            this.p(this.invokeCC(QUERY_TRACK_LAYOUT));
                            break;
                        case "quit":
                        case "exit":
                            this.stopCondition = true;
                            break start;
                        default:
                            // Use command to obtain literal chaincode function to be called + params
                            String[] cmdParts = cmd.split(" ");
                            CC_FUNCTION function = CC_FUNCTION.fromString(cmdParts[0]);
                            if (function == null) {
                                this.p("Invalid command or function: " + cmdParts[0], false);
                                continue;
                            }
                            String[] args = Arrays.copyOfRange(cmdParts, 1, cmdParts.length);
                            this.p(this.invokeCC(function, args));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) throws Exception {
        SemiCentralClient semiCentralClient;

        if (args.length == 3) {
            semiCentralClient = new SemiCentralClient(args[0], args[1], Integer.parseInt(args[2]));
        } else if (args.length == 1) {
            semiCentralClient = new SemiCentralClient(args[0]);
        } else {
            semiCentralClient = new SemiCentralClient("client_settings.json");
        }
        semiCentralClient.run();
    }
}
