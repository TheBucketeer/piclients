package client.SensorReading;

import client.PiClient;
import client.PiClientEventListenerBase;
import org.json.JSONObject;

class SensorEventListener extends PiClientEventListenerBase {
    private SensorClient dadsies;

    SensorEventListener(SensorClient daddy) {
        super();
        this.dadsies = daddy;
    }

    public void handleEvent(JSONObject event) {
        // Do nothing
    }

    public PiClient getClient() {
        return this.dadsies;
    }
}
