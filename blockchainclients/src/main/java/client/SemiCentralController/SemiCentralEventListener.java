package client.SemiCentralController;

import client.PiClient;
import client.PiClientEventListenerBase;
import org.json.JSONObject;

public class SemiCentralEventListener extends PiClientEventListenerBase {
    private SemiCentralClient dadsies;

    SemiCentralEventListener(SemiCentralClient daddy) {
        super();
        this.dadsies = daddy;
    }

    public void handleEvent(JSONObject event) {
    }

    public PiClient getClient() {
        return this.dadsies;
    }

    protected void p(Object o) {
        this.dadsies.p(o);
    }
}
