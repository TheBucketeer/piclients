package client.TrainClient;

import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.time.Duration;
import java.time.Instant;

import static client.PiClient.logToFile;

class TrainClientTalker extends Thread {
    private Socket socket;
    private DataOutputStream msg;
    private TrainClient parent;
    private String commandLogFile;

    TrainClientTalker(String host, int port, TrainClient grilledCheese) {
        // Because who doesn't want a grilled cheese as their parent?
        this.parent = grilledCheese;
        this.commandLogFile = this.parent.getAssetId() + "_commands.txt";
        logToFile(this.commandLogFile, "\n", true);
        try {
            InetAddress h = InetAddress.getByName(host);
            this.socket = new Socket(h, port);
            this.msg = new DataOutputStream(this.socket.getOutputStream());
        } catch (IOException e) {
            this.p("Server not found! (" + e.getMessage() + ")");
            e.printStackTrace();
        }
    }

    private void destroyer() {
        try {
            sleep(10);
            this.socket.close();
            this.parent.talkerHasStopped = true;
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void p(Object o) {
        logToFile(this.commandLogFile, "[TrainClientTalker]: " + o);
    }

    @Override
    public void run() {
        boolean timeout = false;
        Instant start = Instant.now();
        String ping = "{\"type\": \"ping\"}";

        while (!this.parent.stopCondition) {
            try {
                JSONObject message = this.parent.commandQueue.poll();
                if (message != null) {
                    /*
                     * Send message polled from the commandQueue and reset timer
                     * */
                    this.msg.writeBytes(message.toString());
                    this.p(message.toString());
                    start = Instant.now();
                    sleep(10);
                } else if (timeout) {
                    /*
                     * Send ping and reset timer
                     * */
                    this.msg.writeBytes(ping);
                    this.p(ping);
                    timeout = false;
                    start = Instant.now();
                    sleep(10);
                } else {
                    /*
                     * Check if a ping has to be sent the next cycle
                     * */
                    Instant currTime = Instant.now();
                    if (Duration.between(start, currTime).getSeconds() > 1) {
                        timeout = true;
                    }
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
        this.destroyer();
    }
}