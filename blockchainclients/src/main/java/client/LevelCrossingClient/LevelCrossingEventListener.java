package client.LevelCrossingClient;

import client.PiClient;
import client.PiClientEventListenerBase;
import org.json.JSONObject;
import shared.assetdata.LevelCrossingConfig.LC_STATE;
import shared.errors.InvalidLevelCrossingStateException;

class LevelCrossingEventListener extends PiClientEventListenerBase {
    private LevelCrossingClient dadsies;

    LevelCrossingEventListener(LevelCrossingClient daddy) {
        super();
        this.dadsies = daddy;
    }

    public void handleEvent(JSONObject event) {
        boolean openForTrains = event.getBoolean("state");
        try {
            LC_STATE newState = LC_STATE.fromBool(openForTrains);
            this.dadsies.updateState(newState);
        } catch (InvalidLevelCrossingStateException e) {
            this.dadsies.openForTrains();
            this.p(e.getMessage());
            e.printStackTrace();
        }
    }

    public PiClient getClient() {
        return this.dadsies;
    }
}
