package client.SwitchClient;

import client.PiClient;
import client.PiClientEventListenerBase;
import org.json.JSONObject;
import shared.assetdata.SwitchConfig.SWITCH_STATE;
import shared.errors.InvalidSwitchStateException;


class SwitchEventListener extends PiClientEventListenerBase {
    private SwitchClient dadsies;

    SwitchEventListener(SwitchClient daddy) {
        super();
        this.dadsies = daddy;
    }

    public void handleEvent(JSONObject event) throws InterruptedException {
        try {
            this.dadsies.currentDirection = SWITCH_STATE.fromBool(event.getBoolean("state"));
            this.p("Will switch " + this.dadsies.currentDirection.getName() + "!");
            this.dadsies.doSwitch();
            this.dadsies.updateChaincode();
        } catch (InvalidSwitchStateException e) {
            this.p(e.getMessage());
        }
    }

    public PiClient getClient() {
        return this.dadsies;
    }
}
