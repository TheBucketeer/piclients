package client.SignalClient;

import client.PiClient;
import client.PiClientEventListenerBase;
import org.json.JSONObject;
import shared.assetdata.SignalConfig.SIGNAL_ASPECT;
import shared.errors.InvalidSignalAspectException;

class SignalEventListener extends PiClientEventListenerBase {
    private SignalClient dadsies;

    SignalEventListener(SignalClient daddy) {
        super();
        this.dadsies = daddy;
    }

    public void handleEvent(JSONObject event) {
        try {
            this.dadsies.updateAspect(SIGNAL_ASPECT.fromInt(event.getInt("aspect")));
        } catch (InvalidSignalAspectException e) {
            this.p(e.getMessage());
        }
    }

    public PiClient getClient() {
        return this.dadsies;
    }
}
